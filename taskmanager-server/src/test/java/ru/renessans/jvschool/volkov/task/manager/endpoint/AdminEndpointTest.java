package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class AdminEndpointTest {

//    @NotNull
//    private final IAdminEndpoint adminEndpoint = new AdminEndpoint();

//    @Before
//    public void initialAdminRecordBefore() {
//        @NotNull final User addAdminRecord = this.userService.addUser(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addAdminRecord);
//    }
//
//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.serviceLocator.getEntityManagerService().build();
//    }
//
//    @Test
//    @TestCaseName("Run testCloseAllSessions for closeAllSessions(session)")
//    public void testCloseAllSessions() {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        final boolean closeSessions = this.adminEndpoint.closeAllSessions(conversion);
//        Assert.assertTrue(closeSessions);
//    }
//
//    @Test
//    @TestCaseName("Run testGetAllSessions for getAllSessions(session)")
//    public void testGetAllSessions() {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final Collection<SessionDTO> allSessions = this.adminEndpoint.getAllSessions(conversion);
//        Assert.assertNotNull(allSessions);
//        Assert.assertNotEquals(0, allSessions.size());
//    }
//
//    @Test
//    @TestCaseName("Run testSignUpUserWithUserRole for signUpUserWithUserRole(session, \"{0}\", \"{1}\", {2})")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsWithRoleCaseData"
//    )
//    public void testSignUpUserWithUserRole(
//            @NotNull final String login,
//            @NotNull final String password,
//            @NotNull final UserRole userRole
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        Assert.assertNotNull(userRole);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO addUser = this.adminEndpoint.signUpUserWithUserRole(conversion, login, password, userRole);
//        Assert.assertNotNull(addUser);
//        Assert.assertEquals(login, addUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
//        Assert.assertEquals(userRole, addUser.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteUserById for deleteUserById(session, \"{0}\", \"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testDeleteUserById(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO deleteUser = this.adminEndpoint.deleteUserById(conversion, addRecord.getId());
//        Assert.assertNotNull(deleteUser);
//        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
//        Assert.assertEquals(login, deleteUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, deleteUser.getPasswordHash());
//        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteUserByLogin for deleteUserByLogin(session, \"{0}\", \"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testDeleteUserByLogin(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(conversion, login);
//        Assert.assertNotNull(deleteUser);
//        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
//        Assert.assertEquals(login, deleteUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, deleteUser.getPasswordHash());
//        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteAllUsers for deleteAllUsers(session)")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testDeleteAllUsers(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        final boolean deletedAll = this.adminEndpoint.deleteAllUsers(conversion);
//        Assert.assertTrue(deletedAll);
//    }
//
//    @Test
//    @TestCaseName("Run testLockUserByLogin for lockUserByLogin(session, \"{0}\", \"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testLockUserByLogin(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO lockUser = this.adminEndpoint.lockUserByLogin(conversion, login);
//        Assert.assertNotNull(lockUser);
//        Assert.assertEquals(addRecord.getId(), lockUser.getId());
//        Assert.assertEquals(login, lockUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, lockUser.getPasswordHash());
//        Assert.assertEquals(addRecord.getRole(), lockUser.getRole());
//        Assert.assertTrue(lockUser.getLockdown());
//    }
//
//    @Test
//    @TestCaseName("Run testUnlockUserByLogin for unlockUserByLogin(session, \"{0}\", \"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testUnlockUserByLogin(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @Nullable final User lockUser = this.userService.lockUserByLogin(login);
//        Assert.assertNotNull(lockUser);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO unlockUser = this.adminEndpoint.unlockUserByLogin(conversion, login);
//        Assert.assertNotNull(unlockUser);
//        Assert.assertEquals(addRecord.getId(), unlockUser.getId());
//        Assert.assertEquals(login, unlockUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, unlockUser.getPasswordHash());
//        Assert.assertEquals(addRecord.getRole(), unlockUser.getRole());
//        Assert.assertFalse(unlockUser.getLockdown());
//    }
//
//    @Test
//    @TestCaseName("Run testGetAllUsers for getAllUsers(session)")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testGetAllUsers(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final Collection<UserDTO> getUser = this.adminEndpoint.getAllUsers(conversion);
//        Assert.assertNotNull(getUser);
//        Assert.assertNotEquals(0, getUser.size());
//    }
//
//    @Test
//    @TestCaseName("Run testGetAllUsersTasks for getAllUsersTasks(session)")
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetAllUsersTasks(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final Task addRecord = this.taskService.persist(task);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final Collection<TaskDTO> allUsersTasks = this.adminEndpoint.getAllUsersTasks(conversion);
//        Assert.assertNotNull(allUsersTasks);
//        Assert.assertNotEquals(0, allUsersTasks.size());
//    }
//
//    @Test
//    @TestCaseName("Run testGetAllUsersProjects for getAllUsersProjects(session)")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetAllUsersProjects(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final Project addRecord = this.projectService.persist(project);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final Collection<ProjectDTO> allUsersProjects = this.adminEndpoint.getAllUsersProjects(conversion);
//        Assert.assertNotNull(allUsersProjects);
//        Assert.assertNotEquals(0, allUsersProjects.size());
//    }
//
//    @Test
//    @TestCaseName("Run testGetUserById for getUserById(session, \"{0}\", \"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testGetUserById(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO getUser = this.adminEndpoint.getUserById(conversion, addRecord.getId());
//        Assert.assertNotNull(getUser);
//        Assert.assertEquals(addRecord.getId(), getUser.getId());
//        Assert.assertEquals(login, getUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(addRecord.getRole(), getUser.getRole());
//        Assert.assertEquals(hashPassword, getUser.getPasswordHash());
//    }
//
//    @Test
//    @TestCaseName("Run testGetUserByLogin for getUserByLogin(session, \"{0}\", \"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testGetUserByLogin(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO getUser = this.adminEndpoint.getUserByLogin(conversion, addRecord.getLogin());
//        Assert.assertNotNull(getUser);
//        Assert.assertEquals(addRecord.getId(), getUser.getId());
//        Assert.assertEquals(login, getUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(addRecord.getRole(), getUser.getRole());
//        Assert.assertEquals(hashPassword, getUser.getPasswordHash());
//    }
//
//    @Test
//    @TestCaseName("Run testEditProfileById for editProfileById(session, \"{0}\", \"{1}\", \"{2}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsWithNewEntityCaseData"
//    )
//    public void testEditProfileById(
//            @NotNull final String login,
//            @NotNull final String password,
//            @NotNull final String newFirstName
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        Assert.assertNotNull(newFirstName);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO editUser = this.adminEndpoint.editProfileById(conversion, addRecord.getId(), newFirstName);
//        Assert.assertNotNull(editUser);
//        Assert.assertEquals(addRecord.getId(), editUser.getId());
//        Assert.assertEquals(login, editUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, editUser.getPasswordHash());
//        Assert.assertEquals(addRecord.getRole(), editUser.getRole());
//        Assert.assertEquals(newFirstName, editUser.getFirstName());
//    }
//
//    @Test
//    @TestCaseName("Run testUpdatePasswordById for updatePasswordById(session, \"{0}\", \"{1}\", \"{2}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsWithNewEntityCaseData"
//    )
//    public void testUpdatePasswordById(
//            @NotNull final String login,
//            @NotNull final String password,
//            @NotNull final String newPassword
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        Assert.assertNotNull(newPassword);
//        @NotNull final User addRecord = this.userService.addUser(login, password);
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final UserDTO updatePassword = this.adminEndpoint.updatePasswordById(conversion, addRecord.getId(), newPassword);
//        Assert.assertNotNull(updatePassword);
//        Assert.assertEquals(addRecord.getId(), updatePassword.getId());
//        Assert.assertEquals(login, updatePassword.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(newPassword);
//        Assert.assertEquals(addRecord.getRole(), updatePassword.getRole());
//        Assert.assertEquals(hashPassword, updatePassword.getPasswordHash());
//    }

}