package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserUnlimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidDomainException;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final IUserUnlimitedAdapterService userUnlimitedAdapterService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    public DomainService(
            @NotNull final IUserService userService,
            @NotNull final ITaskUserService taskUserService,
            @NotNull final IProjectUserService projectUserService,
            @NotNull final IUserUnlimitedAdapterService userUnlimitedAdapterService,
            @NotNull final ITaskAdapterService taskAdapterService,
            @NotNull final IProjectAdapterService projectAdapterService
    ) {
        this.userService = userService;
        this.taskUserService = taskUserService;
        this.projectUserService = projectUserService;
        this.userUnlimitedAdapterService = userUnlimitedAdapterService;
        this.taskAdapterService = taskAdapterService;
        this.projectAdapterService = projectAdapterService;
    }

    @SneakyThrows
    @Override
    public DomainDTO dataImport(@Nullable final DomainDTO domain) {
        if (Objects.isNull(domain)) throw new InvalidDomainException();
        this.userService.setAllRecords(
                domain.getUsers()
                        .stream()
                        .map(this.userUnlimitedAdapterService::toModel)
                        .collect(Collectors.toList())
        );

        this.taskUserService.setAllRecords(
                domain.getTasks()
                        .stream()
                        .map(this.taskAdapterService::toModel)
                        .collect(Collectors.toList())
        );

        this.projectUserService.setAllRecords(
                domain.getProjects()
                        .stream()
                        .map(this.projectAdapterService::toModel)
                        .collect(Collectors.toList())
        );
        return domain;
    }

    @SneakyThrows
    @Override
    public DomainDTO dataExport(@Nullable final DomainDTO domain) {
        if (Objects.isNull(domain)) throw new InvalidDomainException();
        domain.setUsers(
                this.userService.getAllRecords()
                        .stream()
                        .map(this.userUnlimitedAdapterService::toDTO)
                        .collect(Collectors.toList())
        );
        domain.setTasks(
                this.taskUserService.getAllRecords()
                        .stream()
                        .map(this.taskAdapterService::toDTO)
                        .collect(Collectors.toList())
        );
        domain.setProjects(
                this.projectUserService.getAllRecords()
                        .stream()
                        .map(this.projectAdapterService::toDTO)
                        .collect(Collectors.toList())
        );
        return domain;
    }

}