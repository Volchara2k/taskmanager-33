package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import java.util.Collection;
import java.util.List;

@Repository
public final class TaskUserRepository extends AbstractUserOwnerRepository<Task> implements ITaskUserRepository {

    @NotNull
    @Override
    public Collection<Task> getAll(
            @NotNull final String userId
    ) {
        @NotNull final String jpqlQuery = "FROM Task WHERE user.id = :userId ORDER BY creationDate";
        return super.entityManager.createQuery(jpqlQuery, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final String jpqlQuery = "FROM Task WHERE user.id = :userId ORDER BY creationDate";
        @NotNull final List<Task> tasks = super.entityManager.createQuery(jpqlQuery, Task.class)
                .setParameter("userId", userId)
                .getResultList();
        if (tasks.size() < index) return null;
        return tasks.get(index);
    }

    @Nullable
    @Override
    public Task getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpqlQuery = "FROM Task WHERE user.id = :userId AND id = :id";
        @NotNull final List<Task> tasks = super.entityManager.createQuery(jpqlQuery, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Nullable
    @Override
    public Task getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @NotNull final String jpqlQuery = "FROM Task WHERE user.id = :userId AND title = :title";
        @NotNull final List<Task> tasks = super.entityManager.createQuery(jpqlQuery, Task.class)
                .setParameter("userId", userId)
                .setParameter("title", title)
                .setMaxResults(1)
                .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @NotNull
    @Override
    public Collection<Task> getAllRecords() {
        @NotNull final String jpqlQuery = "FROM Task ORDER BY creationDate";
        return super.entityManager.createQuery(jpqlQuery, Task.class)
                .getResultList();
    }

}