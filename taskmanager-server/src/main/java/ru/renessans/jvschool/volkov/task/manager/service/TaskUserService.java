package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalGetProjectException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTaskException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

@Service
@Transactional
public final class TaskUserService extends AbstractUserOwnerService<Task> implements ITaskUserService {

    @NotNull
    private final IProjectUserService projectUserService;

    @Autowired
    public TaskUserService(
            @NotNull final ITaskUserRepository repository,
            @NotNull final IProjectUserService projectUserService
    ) {
        super(repository);
        this.projectUserService = projectUserService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task add(
            @Nullable final Task task
    ) {
        if (Objects.isNull(task)) throw new InvalidTaskException();
        return super.persist(task);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task add(
            @Nullable final String userId,
            @Nullable final String projectTitle,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(projectTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();

        @Nullable final Project project = this.projectUserService.getByTitle(userId, projectTitle);
        if (Objects.isNull(project)) throw new IllegalGetProjectException();
        @NotNull final Task task = new Task(userId, title, description);
        task.setProject(project);

        return this.add(task);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Task task = new Task(userId, title, description);
        return this.add(task);
    }

}