package ru.renessans.jvschool.volkov.task.manager.configuration;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.renessans.jvschool.volkov.task.manager.api.configuration.IApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DataSourceConst;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.config.InvalidDriverException;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource(value = "classpath:datasource-conf.properties")
@ComponentScan("ru.renessans.jvschool.volkov.task.manager")
public class ApplicationConfiguration implements IApplicationConfiguration {

    @NotNull
    private static final String MODEL_PACKAGE = "ru.renessans.jvschool.volkov.task.manager.model";

    @NotNull
    private final Environment environment;

    public ApplicationConfiguration(
            @NotNull final Environment environment
    ) {
        this.environment = environment;
    }

    @Bean
    @SneakyThrows
    @NotNull
    @Override
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        @Nullable final String driver = this.environment.getProperty(DataSourceConst.DRIVER);
        if (ValidRuleUtil.isNullOrEmpty(driver)) throw new InvalidDriverException();

        dataSource.setDriverClassName(driver);
        dataSource.setUrl(this.environment.getProperty(DataSourceConst.URL));
        dataSource.setUsername(this.environment.getProperty(DataSourceConst.USER));
        dataSource.setPassword(this.environment.getProperty(DataSourceConst.PASS));

        return dataSource;
    }

    @Bean
    @Override
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(MODEL_PACKAGE);
        @NotNull final Properties properties = this.getJpaProperties();
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @Override
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @NotNull
    private Properties getJpaProperties() {
        @NotNull final Properties properties = new Properties();
        properties.put(DataSourceConst.SHOW_SQL, this.environment.getProperty(DataSourceConst.SHOW_SQL));
        properties.put(DataSourceConst.FORMAT_SQL, this.environment.getProperty(DataSourceConst.FORMAT_SQL));
        properties.put(DataSourceConst.HBM2DDL_AUTO, this.environment.getProperty(DataSourceConst.HBM2DDL_AUTO));
        properties.put(DataSourceConst.DIALECT, this.environment.getProperty(DataSourceConst.DIALECT));
        return properties;
    }

}