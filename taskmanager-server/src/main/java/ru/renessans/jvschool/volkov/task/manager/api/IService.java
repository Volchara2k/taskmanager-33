package ru.renessans.jvschool.volkov.task.manager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

import java.util.Collection;

public interface IService<E extends AbstractModel> {

    @NotNull
    E persist(
            @Nullable E value
    );

    @NotNull
    E merge(
            @Nullable E value
    );

    @NotNull
    Collection<E> setAllRecords(
            @Nullable Collection<E> values
    );

    @Nullable
    E deletedRecord(
            @Nullable E value
    );

    boolean deletedAllRecords();

}