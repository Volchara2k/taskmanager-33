package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValuesException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

@Transactional
public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    protected AbstractService(
            @NotNull final IRepository<E> repository
    ) {
        this.repository = repository;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E persist(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        return this.repository.persist(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E merge(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        return this.repository.merge(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> setAllRecords(@Nullable final Collection<E> values) {
        if (ValidRuleUtil.isNullOrEmpty(values)) throw new InvalidValuesException();
        return this.repository.setAllRecords(values);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E deletedRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        return this.repository.deleteRecord(value);
    }

    @SneakyThrows
    @Override
    public boolean deletedAllRecords() {
        return this.repository.deleteAllRecords();
    }

}