package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ITaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.stream.Collectors;

@Component
@WebService
public final class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final ISessionAdapterService sessionAdapterService;

    public TaskEndpoint(
            @NotNull final ITaskUserService taskUserService,
            @NotNull final ITaskAdapterService taskAdapterService,
            @NotNull final ISessionService sessionService,
            @NotNull final ISessionAdapterService sessionAdapterService
    ) {
        this.taskUserService = taskUserService;
        this.taskAdapterService = taskAdapterService;
        this.sessionService = sessionService;
        this.sessionAdapterService = sessionAdapterService;
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO addTaskForProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectTitle", partName = "projectTitle") @Nullable final String projectTitle,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @NotNull final Task task = this.taskUserService.add(current.getUserId(), projectTitle, title, description);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO addTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @NotNull final Task task = this.taskUserService.add(current.getUserId(), title, description);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    @Override
    public TaskDTO updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.updateById(current.getUserId(), id, title, description);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    @Override
    public TaskDTO updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.updateByIndex(current.getUserId(), index, title, description);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @Override
    public TaskDTO deleteTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.deleteById(current.getUserId(), id);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @Override
    public TaskDTO deleteTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.deleteByIndex(current.getUserId(), index);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @Override
    public TaskDTO deleteTaskByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.deleteByTitle(current.getUserId(), title);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "deletedTasks", partName = "deletedTasks")
    @NotNull
    @Override
    public Collection<TaskDTO> deleteAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.taskUserService.deleteAll(current.getUserId())
                .stream()
                .map(this.taskAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @Nullable final Task task = taskUserService.getById(current.getUserId(), id);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.getByIndex(current.getUserId(), index);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Task task = this.taskUserService.getByTitle(current.getUserId(), title);
        return taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @Override
    public Collection<TaskDTO> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.taskUserService.getAll(current.getUserId())
                .stream()
                .map(this.taskAdapterService::toDTO)
                .collect(Collectors.toList());
    }

}