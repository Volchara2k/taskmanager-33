package ru.renessans.jvschool.volkov.task.manager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class TaskDeleteByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_INDEX = "task-delete-by-index";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_INDEX = "удалить задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_INDEX =
            "Происходит попытка инициализации удаления задачи. \n" +
                    "Для удаления задачи по индексу введите индекс задачи из списка. ";

    @Autowired
    public TaskDeleteByIndexListener(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(taskEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_TASK_DELETE_BY_INDEX;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_DELETE_BY_INDEX;
    }

    @Async
    @Override
    @EventListener(condition = "@taskDeleteByIndexListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_DELETE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final TaskDTO delete = super.taskEndpoint.deleteTaskByIndex(current, index);
        ViewUtil.print(delete);
    }

}