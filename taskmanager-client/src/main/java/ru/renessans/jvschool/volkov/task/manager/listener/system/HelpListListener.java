package ru.renessans.jvschool.volkov.task.manager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
public class HelpListListener extends AbstractListener {

    @NotNull
    private static final String CMD_HELP = "help";

    @NotNull
    private static final String ARG_HELP = "-h";

    @NotNull
    private static final String DESC_HELP = "вывод списка команд";

    @NotNull
    private static final String NOTIFY_HELP = "Список команд";

    @NotNull
    private final ICommandService commandService;

    @Autowired
    public HelpListListener(@NotNull final ICommandService commandService) {
        this.commandService = commandService;
    }

    @NotNull
    @Override
    public String command() {
        return CMD_HELP;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_HELP;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_HELP;
    }

    @Async
    @Override
    @EventListener(condition = "@helpListListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_HELP);
        @Nullable final Collection<AbstractListener> commands = this.commandService.getAllCommands();
        ViewUtil.print(commands);
    }

}