package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.stream.Collectors;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Collection<AbstractListener> commandList;

    public CommandRepository(@NotNull final Collection<AbstractListener> commandList) {
        this.commandList = commandList;
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getAllCommands() {
        return commandList;
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getAllTerminalCommands() {
        return commandList
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.command()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getAllArgumentCommands() {
        return commandList
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.argument()))
                .collect(Collectors.toList());
    }

}