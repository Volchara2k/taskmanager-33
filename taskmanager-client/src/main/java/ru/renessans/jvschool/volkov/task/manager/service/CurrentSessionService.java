package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidSessionException;

import java.util.Objects;

@Service
public final class CurrentSessionService implements ICurrentSessionService {

    @NotNull
    private final ICurrentSessionRepository sessionRepository;

    public CurrentSessionService(
            @NotNull final ICurrentSessionRepository sessionRepository
    ) {
        this.sessionRepository = sessionRepository;
    }

    @Nullable
    @SneakyThrows
    @Override
    public SessionDTO getSession() {
        return this.sessionRepository.get();
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO subscribe(
            @Nullable final SessionDTO session
    ) {
        if (Objects.isNull(session)) throw new InvalidSessionException();
        return this.sessionRepository.put(session);
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO unsubscribe() {
        return this.sessionRepository.delete();
    }

}