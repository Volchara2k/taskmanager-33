package ru.renessans.jvschool.volkov.task.manager.listener.admin.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;

public abstract class AbstractAdminDataListener extends AbstractListener {

    @NotNull
    protected final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

    @Autowired
    protected AbstractAdminDataListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.adminDataInterChangeEndpoint = adminDataInterChangeEndpoint;
        this.currentSessionService = currentSessionService;
    }

}