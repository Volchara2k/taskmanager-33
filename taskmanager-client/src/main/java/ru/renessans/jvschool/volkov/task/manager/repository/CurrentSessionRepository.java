package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidSessionException;

import java.util.Objects;

@Repository
public final class CurrentSessionRepository implements ICurrentSessionRepository {

    @Nullable
    private SessionDTO session;

    @NotNull
    @Override
    public SessionDTO put(@NotNull final SessionDTO session) {
        this.session = session;
        return session;
    }

    @Nullable
    @Override
    public SessionDTO get() {
        return this.session;
    }

    @NotNull
    @SneakyThrows
    @Override
    public SessionDTO delete() {
        @Nullable final SessionDTO deleted = get();
        if (Objects.isNull(deleted)) throw new InvalidSessionException();
        this.session = null;
        return deleted;
    }

}